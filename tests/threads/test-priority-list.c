#include "threads/malloc.h"
#include <stdio.h>

#include "lib/kernel/list.h"
#include "listpop.h"

struct list * create_list(void);
void populate(struct list * l, int * a, int n);
bool compare_items (const struct list_elem * a,
                    const struct list_elem * b,
                    void * aux);
void print_sorted(struct list *l);
void test_priority_list(void);

struct item {
    struct list_elem elem;
    int priority;
};

struct list * create_list(void) {

    struct list * item_list = malloc(sizeof(struct list));
    list_init(item_list);
    return item_list;
}

void populate(struct list * l, int * a, int n) {

    int i;
    for(i = 0; i < n; i++) {
        struct item * newitem = malloc(sizeof(struct item));
        newitem->priority = *(a+i);
        list_push_back(l, &newitem->elem);
    }
}

bool compare_items (const struct list_elem * a,
                    const struct list_elem * b,
                    void * aux) {

    struct item * ia = list_entry(a, struct item, elem);
    struct item * ib = list_entry(b, struct item, elem);
    return (ia->priority < ib->priority);
}

void print_sorted(struct list *l) {

    list_sort(l, compare_items, NULL);

    struct list_elem * pos;
    for (pos = list_begin(l);
         pos != list_end(l) ;
         pos = list_next(pos)) {

        struct item * it;
        it = list_entry(pos, struct item, elem);
        printf("priority %d\n", it->priority);
    }

}

void test_priority_list(void) {
    struct list * mylist = create_list();
    populate(mylist, ITEMARRAY, ITEMCOUNT);
    print_sorted(mylist);
}
