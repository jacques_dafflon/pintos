#include "userprog/syscall.h"
#include <stdio.h>
#include <string.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/palloc.h"
#include "threads/malloc.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include "devices/shutdown.h"
#include "devices/input.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "lib/kernel/hash.h"
#include "threads/synch.h"

static void syscall_handler (struct intr_frame *);

typedef void (*handler) (struct intr_frame *);
static void syscall_halt (struct intr_frame *);
static void syscall_exit (struct intr_frame *);
static void syscall_exec (struct intr_frame *);
static void syscall_wait (struct intr_frame *);
static void syscall_create (struct intr_frame *);
static void syscall_remove (struct intr_frame *);
static void syscall_open (struct intr_frame *);
static void syscall_filesize (struct intr_frame *);
static void syscall_read (struct intr_frame *);
static void syscall_write (struct intr_frame *);
static void syscall_seek (struct intr_frame *);
static void syscall_tell (struct intr_frame *);
static void syscall_close (struct intr_frame *);
static bool check_user_address (void *);
static void exit(int status);
static int gen_fd (void);
static struct file * get_file_from_fd(int);
static struct item * get_item_from_fd(int);
unsigned item_hash (const struct hash_elem *, void *);
bool item_less (const struct hash_elem *, const struct hash_elem *, void *);

#define SYSCALL_MAX_CODE 19
static handler call[SYSCALL_MAX_CODE + 1];

static struct lock filesys_lock;

struct item
{
  int fd;
  struct file * file;
  struct thread * owner;
  struct hash_elem elem;
};

struct hash items;

void
syscall_init (void)
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");

  /* Any syscall not registered here should be NULL (0) in the call array. */
  memset(call, 0, SYSCALL_MAX_CODE + 1);

  /* Check file lib/syscall-nr.h for all the syscall codes and file
   * lib/user/syscall.c for a short explanation of each system call. */
  call[SYS_HALT]  = syscall_halt;         /* Halt the operating system. */
  call[SYS_EXIT]  = syscall_exit;         /* Terminate this process. */
  call[SYS_EXEC]  = syscall_exec;         /* Start another process. */
  call[SYS_WAIT]  = syscall_wait;         /* Wait for a child process to die. */
  call[SYS_CREATE]  = syscall_create;     /* Create a file. */
  call[SYS_REMOVE]  = syscall_remove;     /* Delete a file. */
  call[SYS_OPEN]  = syscall_open;         /* Open a file. */
  call[SYS_FILESIZE]  = syscall_filesize; /* Obtain a file's size. */
  call[SYS_READ]  = syscall_read;         /* Read from a file. */
  call[SYS_WRITE]  = syscall_write;       /* Write to a file. */
  call[SYS_SEEK]  = syscall_seek;         /* Change position in a file. */
  call[SYS_TELL]  = syscall_tell;       /* Report current position in a file. */
  call[SYS_CLOSE]  = syscall_close;       /* Close a file. */

  lock_init (&filesys_lock);

  hash_init(&items, item_hash, item_less, NULL);

}

static void
syscall_handler (struct intr_frame *f)
{
  int *syscall_code;
  syscall_code = f->esp;

  if (!check_user_address(syscall_code) ||           //Stack pointer
      !(check_user_address(syscall_code + 1)         //Arg 1
        && check_user_address(syscall_code + 2)      //Arg 2
        && check_user_address(syscall_code + 3)) ||  //Arg3
      *syscall_code < SYS_HALT || *syscall_code > SYS_CLOSE) { //Supported calls
    exit(-1);

  } else {
    call[*syscall_code](f);
  }
}

static void
syscall_halt (struct intr_frame *f)
{
  shutdown_power_off();
}

static void
syscall_exit (struct intr_frame * f)
{
  int *stack = f->esp;

  exit((int) *(stack+1));
}

static void
syscall_exec (struct intr_frame * f)
{
  int * stackpointer = f->esp;
  char * command = (char *) *(stackpointer + 1);

  if (check_user_address (command)) {
    //lock_acquire (&filesys_lock);
    f->eax = process_execute (command);
    //lock_release(&filesys_lock);
  } else {
    f->eax = -1;
  }
}

static void
syscall_wait (struct intr_frame * f)
{
  int * stack = (void *) f->esp;
  tid_t child_tid = *(stack+1);
  if (check_user_address(f->esp))
    f->eax = process_wait (child_tid);
  else
    exit(-1);
}

static void
syscall_create (struct intr_frame * f)
{
  int *stack = f->esp;
  char * file = (char *) *(stack+1);
  if (check_user_address(file) )
    f->eax = filesys_create(file, *(stack+2));
  else {
    exit(-1);
  }
}

static void
syscall_remove (struct intr_frame * f)
{
  int *stack = f->esp;
  char * file = (char *) *(stack + 1);
  if (check_user_address(file) )
    f->eax = filesys_remove(file);
  else {
    exit(-1);
  }
}

static void
syscall_open (struct intr_frame * f)
{
  int *stack = f->esp;
  char * filename = (char *) *(stack+1);
  struct file * file;
  struct item * fd_item;

  if (! filename) {
    f->eax = -1;
    return;
  }

  if (!check_user_address (filename)) {
    exit(-1);
  }

  file = filesys_open(filename);
  if (file == NULL) {
    f->eax = -1;
    return;
  }

  fd_item = (struct item * ) malloc (sizeof (struct item));
  if (!fd_item) {
    file_close(file);
    exit(-1);
  }

  fd_item->file = file;
  fd_item->fd = gen_fd();
  fd_item->owner = thread_current();
  hash_insert(&items, &fd_item->elem);

  f->eax = fd_item->fd;

}

static void
syscall_filesize (struct intr_frame * f)
{
  int * stack = f->esp;
  int fd = *(stack+1);

  struct file * file;

  file = get_file_from_fd(fd);
  if (file) {
    f->eax = file_length(file);
  } else {
    f->eax = -1;
  }
}

static void
syscall_read (struct intr_frame * f)
{
  int * stack = f->esp;
  int fd = (int) *(stack+1);
  void * buffer = (void *) *(stack+2);
  unsigned length = (unsigned) *(stack+3);

  unsigned i;

  struct file * file;
  lock_acquire(&filesys_lock);

  if (fd == STDOUT_FILENO) {
    f->eax = -1;

  } else if (!check_user_address(buffer) || !check_user_address(buffer + length)) {
    lock_release (&filesys_lock);
    exit(-1);
    return;

  } else if (fd == STDIN_FILENO) {
    for (i = 0; i < length; i++)  {
      *(uint8_t * )(buffer + i) = input_getc ();
    }

    f->eax = length;

  } else {
    file = get_file_from_fd(fd);
    if (file) {
      int read = file_read(file, buffer, length);
      f->eax = read;

    } else
      f->eax = -1;
  }

  lock_release (&filesys_lock);
}

static void
syscall_write (struct intr_frame * f)
{
  int *stack = f->esp;
  int fd = (int) *(stack+1);
  void * buffer = (void *) *(stack+2);
  unsigned length = (unsigned) *(stack+3);

  struct file * file;
  lock_acquire(&filesys_lock);

  if (fd == STDIN_FILENO) {
    f->eax = 0;

  } else if (fd == STDOUT_FILENO) {
    putbuf(buffer, length);
    f->eax = 1;

  } else if (!check_user_address(buffer)
             || !check_user_address(buffer + length)) {
      lock_release (&filesys_lock);
      exit(-1);
      return;

  } else {
      file = get_file_from_fd (fd);
      if (file) {
        f->eax = (int) file_write (file, buffer, length);
      } else {
        f->eax = -1;
      }
  }

  lock_release (&filesys_lock);
}

static void
syscall_seek (struct intr_frame * f)
{
  int *stack = f->esp;
  int fd = (int) *(stack+1);
  unsigned pos = (unsigned) *(stack+2);

  struct file * file;
  file = get_file_from_fd(fd);

  if (!file) {
    f->eax = -1;
  } else {
    file_seek(file, pos);
    f->eax = 0;
  }
}

static void
syscall_tell (struct intr_frame * f)
{
  int *stack = f->esp;
  int fd = (int) *(stack+1);

  struct file * file;
  file = get_file_from_fd(fd);

  if (!file) {
    f->eax = -1;
  } else {
    file_tell(file);
    f->eax = 0;
  }
}

static void
syscall_close (struct intr_frame * f)
{
  int * stack = f->esp;
  int fd = (int) *(stack+1);

  struct item * fd_item;
  fd_item = get_item_from_fd(fd);
  if (fd_item && fd_item->owner == thread_current()) {
    file_close(fd_item->file);
    hash_delete(&items, &fd_item->elem);
    free(fd_item);
  }

  f->eax = 0;
}

static bool check_user_address (void * ptr) {
  return ptr != NULL && is_user_vaddr (ptr) && pagedir_get_page (thread_current ()->pagedir, ptr);
}

static void exit (int status)
{
  struct thread* t = thread_current ();
  file_close(t->file);
  t->exit_status = status;
  thread_get_child_data(t->parent, t->tid)->exit_status = t->exit_status;
  thread_exit();
}

static int
gen_fd (void)
{
  static int fd = 3;
  return fd++;
}

static struct file *
get_file_from_fd(int fd)
{
  struct item * fd_item;
  fd_item = get_item_from_fd(fd);

  return !fd_item ? NULL : fd_item->file;
}

static struct item *
get_item_from_fd(int fd)
{
  struct item tmp;
  struct hash_elem *e;
  tmp.fd = fd;
  e = hash_find(&items, &tmp.elem);

  if (!e)
    return NULL;

  struct item *entry = hash_entry(e, struct item, elem);
  return entry;
}

// Hash table functions

unsigned item_hash (const struct hash_elem * p, void *aux)
{
  struct item *i = hash_entry (p, struct item, elem);
  return hash_int(i->fd);
}

bool item_less (const struct hash_elem * a, const struct hash_elem * b, void *aux)
{
  struct item *a_entry = hash_entry (a, struct item, elem);
  struct item *b_entry = hash_entry (b, struct item, elem);
  return a_entry->fd < b_entry->fd;
}
